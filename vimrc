" load pathogen
execute pathogen#infect()
execute pathogen#helptags()

syntax on
filetype plugin indent on

" space, tab and newline highlighting
scriptencoding utf-8
nmap <leader>l :set list!<CR>
set listchars=tab:›\ ,trail:·,extends:>,precedes:<,eol:¬

" color scheme
colorscheme darkburn

" disable folding for markdown
let g:vim_markdown_folding_disabled=1

" show line numbers
set number

" toggle line numbers
nmap <F12> :set invnumber<CR>

" tab size
set tabstop=4

" convert tabs to spaces
nmap <leader>ct :set et<Bar>retab<CR>

" convert spaces to tabs
nmap <leader>cs :set noet<Bar>retab!<CR>

" airline/powerline fonts
let g:airline_powerline_fonts = 1

" disable read-only mode for vimdiff
set noro

" Netrw disable
" Stop NERDTree from hijacking netrw
let g:NERDTreeHijackNetrw = 0
let g:loaded_netrw        = 1
let g:loaded_netrwPlugin  = 1

" NERDTree
let g:nerdtree_tabs_open_on_console_startup = 1
let g:nerdtree_tabs_open_on_gui_startup     = 1
let g:nerdtree_tabs_no_startup_for_diff     = 1
let g:nerdtree_tabs_startup_cd              = 1
let g:nerdtree_tabs_focus_on_files          = 1

" NERDTree Keyboard mapping
map <C-S-F12> :NERDTreeToggle<CR>

" Multiple-Cursors mapping
let g:multi_cursor_use_default_mapping=0

let g:multi_cursor_next_key='<C-n>'
let g:multi_cursor_prev_key='<C-p>'
let g:multi_cursor_skip_key='<C-x>'
let g:multi_cursor_quit_key='<Esc>'

" Vim Keyboard mapping
" Close all Windows and tabs at once
map <C-x> :xa<CR>

" Bubble single lines
nmap <C-Up> [e
nmap <C-Down> ]e

" Bubble multiple lines
vmap <C-Up> [egv
vmap <C-Down> ]egv

" tagbar Keyboard mapping
nmap <F8> :TagbarToggle<CR>

" Command-T Keyboard mapping
nmap <C-p> :CommandT<CR>

set t_Co=256
let g:NERDTreeDirArrows=0
let g:airline_powerline_fonts = 1
let g:Powerline_symbols = "fancy"
